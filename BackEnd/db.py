from sqlalchemy import create_engine
from sqlalchemy.orm import declarative_base
from sqlalchemy.orm.session import sessionmaker

#string de coneccion a bd completar con los datos de la bd propia
# SQLALCHEMY_DATABASE_URL = "postgresql+psycopg2://postgres:6284mati@localhost/lab4"
SQLALCHEMY_DATABASE_URL = "postgresql+psycopg2://postgres:6284mati@localhost/lab4"

engine = create_engine(SQLALCHEMY_DATABASE_URL, echo=True)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()

def create_all():    
    Base.metadata.create_all(bind=engine)

def drop_all():
    Base.metadata.drop_all(bind=engine)

def get_session():
    session = SessionLocal()
    try:
        yield session
    finally:
        session.close()
